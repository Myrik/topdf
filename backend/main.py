import uvicorn
from dramatiq.results import ResultMissing
from fastapi import FastAPI, File
from starlette.requests import Request
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from pydantic import BaseModel

from actors import from_url, backend, from_file

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

class Resp(BaseModel):
    message_id: str = None
    error: str = None
    reason: dict = None

@app.get("/")
def read_root(request: Request):
    return templates.TemplateResponse("main.html", {'request': request})


@app.get("/url", response_model=Resp)
def ask_url(path: str) -> Resp:
    message = from_url.send(path)
    return Resp(message_id=message.message_id)


@app.get('/url/{message_id}', response_model=Resp)
def check_url(message_id: str) -> Resp:
    message = from_url.message().copy(message_id=message_id)
    try:
        result = message.get_result(backend=backend)
    except ResultMissing:
        return Resp(error='No results')
    return Resp(**result)


@app.post("/upload", response_model=Resp)
def create_file(file: bytes = File(...)) -> Resp:
    message = from_file.send(file.decode('utf-8'))
    return Resp(message_id=message.message_id)


@app.get('/upload/{message_id}', response_model=Resp)
def check_file(message_id: str) -> Resp:
    message = from_file.message().copy(message_id=message_id)
    try:
        result = message.get_result(backend=backend)
    except ResultMissing:
        return Resp(error='No results')
    return Resp(**result)


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)
