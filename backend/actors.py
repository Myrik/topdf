from uuid import uuid4

import dramatiq
import img2pdf
import os
import requests
from dramatiq.brokers.redis import RedisBroker
from dramatiq.results import Results
from dramatiq.results.backends import RedisBackend
from weasyprint import HTML

redis_address = {
    'host': os.environ.get('REDIS_HOST', '127.0.0.1'), 
    'port': int(os.environ.get('REDIS_PORT', '6379'))
}
splash_endpoint = os.environ.get('SPLASH_HOST', 'http://localhost:8050') + '/render.jpeg'

redis_broker = RedisBroker(**redis_address)

backend = RedisBackend(**redis_address)
results = Results(backend=backend, result_ttl=1000 * 60)
redis_broker.add_middleware(results)
dramatiq.set_broker(redis_broker)


@dramatiq.actor(store_results=True, time_limit=1000 * 30)
def from_file(file: str):
    uuid = str(uuid4())

    pdf = HTML(string=file)

    with open(f'static/{uuid}.pdf', 'wb') as out:
        out.write(pdf.write_pdf())

    return {'message_id': uuid}


@dramatiq.actor(store_results=True, time_limit=1000 * 30)
def from_url(url):
    response = requests.get(
        splash_endpoint,
        params={
            'url': url,
            'render_all': 1,
            'wait': 3
        }
    )

    content_type = response.headers['Content-Type']
    if 'application/json' in content_type:
        return {
            'error': 'invalid response',
            'reason': response.json()
        }
    elif 'text/html' in content_type:
        return {
            'error': 'unknown error',
            'reason': {
                'html': response.text,
                'code': response.status_code
            }
        }

    uuid = str(uuid4())

    with open(f'static/{uuid}.pdf', 'wb') as out:
        out.write(img2pdf.convert(response.content))

    return {'message_id': uuid}
